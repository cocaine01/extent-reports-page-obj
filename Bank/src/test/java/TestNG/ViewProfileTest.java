package TestNG;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Bank.Bank.Base;
import Bank.Bank.LandingPage;
import Bank.Bank.LoginPage;
import Bank.Bank.ViewProfilePage;

public class ViewProfileTest extends Base {
	
	LandingPage landingPageObj = null;
	ViewProfilePage viewProfilePageObj =  null;
	
 
  @BeforeClass
  public void beforeTest() {
	  init();
	  driver.get(CONFIG.getProperty("url"));
  }
  
  
  @Test
  public void Test01() {
	  
	  LoginPage loginPageObj =new LoginPage(driver);
	  landingPageObj = loginPageObj.Login(CONFIG.getProperty("userName"), CONFIG.getProperty("pass"));
	  child.info("Login done");
	  
  }
  
  @Test
  public void Test02(){
	  viewProfilePageObj = landingPageObj.viewProfile();
	  Assert.assertNotNull(viewProfilePageObj);
  }

  @AfterClass
  public void afterClass(){
	  driver.quit();
  }
  
  
}
