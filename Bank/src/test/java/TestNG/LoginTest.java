package TestNG;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Bank.Bank.LandingPage;
import Bank.Bank.LoginPage;
import Bank.Bank.Base;

public class LoginTest extends Base {

	@BeforeClass
	public void beforeTest() {
		init();
		driver.get(CONFIG.getProperty("url"));
	}
  
	@Test
	public void Test00() {
		LoginPage loginPageObj = new LoginPage(driver);
		LandingPage landingPageObj = loginPageObj.Login(CONFIG.getProperty("userName"), CONFIG.getProperty("pass")); 
		child.info("Details of the credentials added");
		Assert.assertNotNull(landingPageObj, "Landing page should be displayed");
		
	}
	
	@AfterClass
	public void afterClass(){
		driver.quit();
	}

}
