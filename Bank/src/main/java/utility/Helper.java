package utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helper{
	
	public static boolean validateElementIsDisplayed(WebDriver driver, String xPath){
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
		if(ele.isDisplayed()){
		return true;}
		else{
			return false;
		}
		
		
	}
	
	
	

}
