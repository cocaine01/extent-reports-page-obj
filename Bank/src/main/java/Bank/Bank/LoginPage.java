package Bank.Bank;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {
    
	WebDriver driver;
	
//	@FindBy(xpath="//input[@id='txtUserName']")
//	WebElement user_name;//	
//	@FindBy(xpath="//input[@id='txtPassword']")
//	WebElement password;//	
//	@FindBy(xpath="//input[@type='submit']")
//	WebElement Login_Button;	
//	public SIPHOMEPAge(WebDriver driver){
//		PageFactory.initElements(driver, this);
//	}//	
//	public void Login(String u_name , String pwd){
//		user_name.sendKeys(u_name);
//		password.sendKeys(pwd);
//		Login_Button.click();
//	}
//	@FindBy(xpath="")
//	WebElement
	
	@FindBy(xpath="//input[@name='username']")
	WebElement email_id;
	
	@FindBy(xpath="//input[@name='password']")
	WebElement Password;
	
	@FindBy(xpath="//input[@type='submit']")
	WebElement LoginSubmit;
	
	public LoginPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public LandingPage Login(String email, String pwd){
		try{
			email_id.sendKeys(email);
			Password.sendKeys(pwd);
		    Base.child.info("Credentials added");
			Base.captureScreenshot();
			
			LoginSubmit.click();
			Base.captureScreenshot();
			return new LandingPage(driver);}
		catch(Exception e){
			return null;
		}
		
		
	}




	
	
	
}
