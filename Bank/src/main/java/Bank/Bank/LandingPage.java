package Bank.Bank;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;



import utility.Helper;

public class LandingPage {
   WebDriver driver;
   
   @FindBy(how = How.XPATH, using = "//a[@href='/dashboard/repositories']") 
   WebElement Repositories;
   
   @FindBy(xpath= "//*[@id='adg3-navigation']/descendant::span[@class = 'hXYiff']/span")
   WebElement userIconBtn;
   
   @FindBy(xpath="//span[text()='View profile']")
   WebElement viewProfilelink;
   
   public LandingPage(WebDriver driver){
	   this.driver =  driver;
	   PageFactory.initElements(driver, this);
	   Assert.assertTrue(Helper.validateElementIsDisplayed(driver, "//div[@id='content']/header/descendant::h1"));
   }
   
      
   public ViewProfilePage viewProfile(){
		userIconBtn.click();		
		viewProfilelink.click();
		return new ViewProfilePage(driver);
	}
   
}
