package Bank.Bank;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;






import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Base {

	public static WebDriver driver = null;
	public static Properties CONFIG = null;
	public ExtentReports  extent = null; //reporter
	public ExtentTest parent =  null; //Left hand side
	public static ExtentTest child =  null;
	
	
	
	
	
	public void openBrowser(String browserType){
		
		if(browserType.equalsIgnoreCase("Chrome")){		    
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			driver= new ChromeDriver();			
		}		
		if(browserType.equalsIgnoreCase("Mozilla")){		       
			driver= new FirefoxDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(Integer.parseInt(CONFIG.getProperty("implicitWaitTime")), TimeUnit.SECONDS);
	}
	
	public void loadProperities(){
		try {
			FileInputStream fis =  new FileInputStream(System.getProperty("user.dir")+ "\\config\\config.properties");
			
			CONFIG = new Properties(); 
			CONFIG.load(fis);
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void init(){
		loadProperities();
		openBrowser(CONFIG.getProperty("browserType"));
	}
	
	//Screen Shot Generic Method
    public static Random random=new Random(10000);//	
	public static ArrayList<String> ss_paths =new ArrayList<String>();
	
	public static String captureScreenshot() {
		TakesScreenshot ts = ((TakesScreenshot)driver);//TakeScreenshot is an interface
		File raw=ts.getScreenshotAs(OutputType.FILE);
		//Declaring the name of the SS and also the path where it will be sttored
		int name = random.nextInt();
		String path= System.getProperty("user.dir")+"\\test-output-extent\\screenshots\\" + name + ".jpg";
		ss_paths.add(path);
		try {
			FileUtils.copyFile(raw, new File(path));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return path;
	}
	
	//Extent reports implemenation
	@BeforeSuite
	public void beforeSuite() {
		extent = new ExtentReports();
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "\\test-output-extent\\extent.html");
		extent.attachReporter(htmlReporter);
	}
	
    @BeforeClass
    public synchronized void beforeClass() {
        parent = extent.createTest(getClass().getName());
    }

    @BeforeMethod
    public synchronized void beforeMethod(Method method) {
        child = parent.createNode(method.getName());
    }

    @AfterMethod
    public synchronized void afterMethod(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE){
        	child.fail(result.getThrowable());
        	Iterator<String> itr =  ss_paths.iterator();
    	while(itr.hasNext()){
    		try {
				child.addScreenCaptureFromPath((String)itr.next());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	ss_paths.clear();///clearing the ss
    }
        else if (result.getStatus() == ITestResult.SKIP)
            child.skip(result.getThrowable());
        else{
            child.pass("Test passed");
        	Iterator<String> itr =  ss_paths.iterator();
        	while(itr.hasNext()){
        		try {
					child.addScreenCaptureFromPath((String)itr.next());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        	ss_paths.clear();
        }	
    }
    
    @AfterSuite
    public synchronized void afterSutie() {
    	extent.flush();
    }
    
    
    
    
}

	
	

