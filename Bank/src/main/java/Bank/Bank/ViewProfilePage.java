package Bank.Bank;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import utility.Helper;

public class ViewProfilePage {
	WebDriver driver ;

	public 	ViewProfilePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
		assertTrue(Helper.validateElementIsDisplayed(driver, "//*[@id='content']/header/div[1]/div/h1"));
	}


}
